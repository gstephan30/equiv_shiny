# equiv_shiny

for testing please on your PC, please install the following packages in R:

`install.packages('tidyverse')`

`install.packages('shiny')`

# Deployment

App is currently deployed under:

https://helmholtz-epid.shinyapps.io/equiv_shiny/

# Script

Script that run the app are:

[server.R](https://gitlab.com/gstephan30/equiv_shiny/blob/master/server.R) & [ui.R](https://gitlab.com/gstephan30/equiv_shiny/blob/master/ui.R)

